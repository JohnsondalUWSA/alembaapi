<?php
    $str = file_get_contents("data.json");

    $json = json_decode($str,true);


?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <script src="https://kit.fontawesome.com/047df7eac9.js" crossorigin="anonymous"></script>
    <title>ASM Manufacture Models</title>
</head>
<body>


<table class="table">
    <thead>
    <tr><th></th>
        <th>ID</th>
        <th>Manufacture</th>
        <th>Models</th>
        <th>Comments</th>

    </tr>

    </thead>
    <tbody>


        <?php
       foreach ($json["Sheet1"] as $item) {
           echo ("<tr>");

           echo "<td><a href='#' data-id='". $item["recordID"]."'><i class='fas fa-pencil-alt'></i></a> </td>";
           echo "<td>".$item["recordID"]."</td>";
           echo "<td>".$item["Manufacture"]."</td>";
           echo "<td>".$item["Models"]."</td>";
           echo "<td>".$item["Comments"]."</td>";
           echo ("</tr>");
       }
        ?>




    </tbody>


</table>


</body>


</html>
