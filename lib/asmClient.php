<?php


class asmClient
{


    public $asm_client_id;
    public $asm_username;
    public $asm_password;

    public $asm_auth;
    public $asm_api;
    public $asm_org;

    public $connected = false;
    public $errMsg = "";

    private $token;

// Constructor - set local parms and url encode values for curls
    public function __construct($client_id, $username, $password, $authURL, $apiURL, $Org)
    {
        $this->asm_client_id = rawurlencode($client_id);
        $this->asm_username = rawurlencode( $username);
        $this->asm_password = rawurlencode($password);
        $this->asm_auth = $authURL;
        $this->asm_api = $apiURL;

        $this->asm_org = $Org;



        $this->getToken();

    }

    private function getToken()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->asm_auth,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=password&username='.$this->asm_username.'&password='.$this->asm_password.'&scope=session-type%3AAnalyst&client_id='.$this->asm_client_id."&partition",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $json = curl_exec($curl);

        if ($json === false)
        {
            $this->errMsg = curl_error(($curl));
            curl_close($curl);
            return;
        }

        curl_close($curl);


        $this->token = json_decode($json);
        $this->connected = true;

}

    public function Query($select , $resource, $filter, $partitionId)
    {
        // add code to validate token

        $queryURL = $this->asm_api . $resource .'?$select='.$select;

        if ($this->asm_org != 0)
        {
            if ($filter != null)
                $queryURL = $queryURL.'&$filter='.$filter.'&&Organization='.$this->asm_org;
            else $queryURL = $queryURL.'&$filter=Organization='.$this->asm_org;
        }else
        {
            if ($filter !=null)
                $queryURL = $queryURL.'&$filter='.$filter;

        }

        if ($partitionId != '*')
        {
            $queryURL = $queryURL.'$partition='.$partitionId;

        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>$queryURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$this->token->access_token
            ),
        ));


        $response = curl_exec($curl);

        if ($response === false)
        {
            $this->errMsg = 'Curl Error - '.curl_error(($curl));
            curl_close($curl);
            return '{"Error": "Error processing API"}';
        }

        curl_close($curl);
        return $response;


    }




    public function Logout()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => str_replace("login","logout",$this->asm_auth),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "token=".$this->token->refresh_token,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$this->token->access_token,
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);

        if ($response === false)
        {
            $this->errMsg = "Curl Error - ".curl_error(($curl));
            curl_close($curl);
            return '{"Error": "Error logging Out"}';
        }

        curl_close($curl);
        return $response;


    }

    public function HasRights ($id, $resource,$partitionId )
    {
        if ($id == 0)
            return true;
        if ($id == -1)
            return true;

        $curl = curl_init();
        $queryURL = $this->asm_api.$resource."/".$id;

        if ($partitionId != "*")
            $queryURL = $queryURL.'&$partition='.$partitionId;


        curl_setopt_array($curl, array(
            CURLOPT_URL => $queryURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',

            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$this->token->access_token

            ),
        ));

        $response = curl_exec($curl);

        if ($response === false)
        {
            $this->errMsg = 'Curl Error - '.curl_error(($curl));
            curl_close ($curl);
            return '{"Error": "Error processing API"}';
        }

        curl_close($curl);

        $ob = json_decode($response);

        if ($this->asm_org != $ob->Organization)
            return false;

        return true;


    }

    public function RunAction ($id,$method,  $resource, $partitionId , $url, $payload)
    {

        $curl = curl_init();
        $queryURL = $this->asm_api.$resource;

        if ($id != "-1")
            $queryURl = $queryURL."/".$id;

        if ($url != '*')
            $queryURL = @$queryURL."/".$url;




        if ($partitionId != "*")
            $queryURL = $queryURL.'?partition='.$partitionId;

        $pay = "";
        if (isset($payload)) $pay = $payload;

     //   $pay = '{ "Ext_UwOsBuild":"-1"}';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $queryURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,


           CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS =>$pay,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$this->token->access_token,
                'Content-Type: application/json'
            ),
        ));


        $response = curl_exec($curl);


        if ($response === false)
        {
            $this->errMsg = 'Curl Error - '.curl_error(($curl));
            curl_close($curl);
            return '{"Error": "Error processing API"}';
        }

        curl_close($curl);
        return $response;
    }
}