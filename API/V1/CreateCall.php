<?php

use Analog\Analog;
use Analog\Handler\PDO;

//Composer Load
require '../../vendor/autoload.php';

//Application Settings Load
require_once('../../settings.php');

//Encryption Key and Lib
require_once('../../lib/encrypt/key.php');
require_once('../../lib/encrypt/uws_encrypt.php');

//ASM Client Class
require_once('../../lib/asmClient.php');


//Retrieve Post Headers - Used to find bearer token
$headers = apache_request_headers();


//Post Variables
$id = null;
$resource = null;
$url = null;
$payload = null;
$method = null;

// Decrypt mySQL Password
$decodedPassword = uws_encrypt::encrypt_decrypt('decrypt', $mysql_password, $mykey);

// Init Analog Loggging Agent
logInit($mysql_server, $mysql_db, $mysql_user, $decodedPassword);


// Confirm that Auth Has been sent
if (!isset($headers["Authorization"])) {
    Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt", ANALOG::URGENT);
    http_response_code(401);
    exit();
}

// Set Filter Variable, not requried
if (isset($_POST["payload"]))
    $payload = $_POST["payload"];

$person = "";
if (isset($_POST["person"]))
    $person = $_POST["person"];
$personF = "";
if (isset($_POST["personSearchField"]))
    $personF = $_POST["personSearchField"];
else if ($person != "") {
    Analog::log(getUserIpAddr() . " Must provide field to search person by", ANALOG::URGENT);
    http_response_code(401);
    exit();

}

$personM = "ignore";
if (isset($_POST["personMatch"]))
    $personM = $_POST["personMatch"];

if ($personM == 'ignore' || $personM == 'first' || $personM == 'single') {
    $a = "good";
} else {
    Analog::log(getUserIpAddr() . "Person Match must be one of the following 'ignore', 'first','single'", ANALOG::URGENT);
    echo "Person Match must be one of the following 'ignore', 'first','single'";
    http_response_code(401);
    exit();

}

// Set MySQL Info - https://github.com/SergeyTsalkov/meekrodb
DB::$host = $mysql_server;
DB::$user = $mysql_user;
DB::$password = $decodedPassword;
DB::$dbName = $mysql_db;


//encrypt token that has been set via Bearer auth.  DB stores the token in encrypted state.
$encryptedToken = uws_encrypt::encrypt_decrypt('encrypt', str_replace("Bearer ", "", $headers["Authorization"]), $mykey);


//Retrieve Token from DB using encryptedToken
$tokenInfo = DB::queryFirstRow("SELECT * FROM `tokens` where token = '" . $encryptedToken . "'");

//Is Token in DB?
if ($tokenInfo == null) {
    echo("Bad Token!");
    Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt - BAD Token", ANALOG::URGENT);
    http_response_code(401);
    exit();
}

$isTest = true;
if ($tokenInfo["isTest"] == "0")
    $isTest = false;

if (!$isTest) {
    $asm_auth = $asmclient["prod"]->auth;
    $asm_api = $asmclient["prod"]->api;
    $asm_username = $asmclient["prod"]->partitions[$tokenInfo["PartitionId"]]->username;
    $asm_password = $asmclient["prod"]->partitions[$tokenInfo["PartitionId"]]->password;
    $asm_client_id = $asmclient["prod"]->client_id;
} else {
    $asm_auth = $asmclient["test"]->auth;
    $asm_api = $asmclient["test"]->api;
    $asm_username = $asmclient["test"]->partitions[$tokenInfo["PartitionId"]]->username;
    $asm_password = $asmclient["test"]->partitions[$tokenInfo["PartitionId"]]->password;
    $asm_client_id = $asmclient["test"]->client_id;
}

//IS Token Active?
if ($tokenInfo["ACTIVE"] != "1") {
    Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt - Expired Token", ANALOG::URGENT);
    echo("Token is expired!");
    http_response_code(401);
    exit();
}

//Is Token IP SCOPED
if ($tokenInfo["IP_FILTER"] != "0.0.0.0") {

    $remoteIP = getUserIpAddr();
    if ($remoteIP == "::1")
        $remoteIP = '127.0.0.1';

    if ($remoteIP != $tokenInfo["IP_FILTER"]) {
        Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt - Invalid IP", ANALOG::URGENT);
        echo("No Access!");
        http_response_code(401);
        exit();
    }
}

if ($tokenInfo["CanCreateCall"] == "no") {
    Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt", ANALOG::URGENT);
    echo("No Access!");
    http_response_code(401);
    exit();


}


$response = "";

// INIT ASM CLIENT - ASM Client will init login auth with Alemba
$client = new asmClient($asm_client_id, $asm_username,
    uws_encrypt::encrypt_decrypt('decrypt', $asm_password, $mykey),
    $asm_auth, $asm_api, $tokenInfo["INSTITUTION_ID"]);

// Log Users Query in DB
Analog::log(getUserIpAddr() . " org=" . $tokenInfo["INSTITUTION_ID"] . ",tokenID=" . $tokenInfo["ID"] . ", method=" . 'call' . ", resource=*, id=* , url=* ,payload=" . (isset($payload)) ? $payload : "", Analog::DEBUG);

// If client is connected run query, log if not online.
if ($client->connected) {
    $payloadO = json_decode($payload);
    if ($payloadO == null) {
        Analog::log(getUserIpAddr() . " Bad Payload", ANALOG::URGENT);
        echo("Bad Payload No Access!");
        http_response_code(401);
        exit();
    }

    $client->asm_org = 0;


    if ($person != "") {

        $SearchResult = $client->Query("Ref", "user", $personF . '=="' . $person . '"', '*');
        $SearchResultO = json_decode($SearchResult);
        if ($SearchResultO != null) {
            if (count($SearchResultO->results) > 0) {
                if (count($SearchResultO->results) > 1 && $personM == 'single') {
                    Analog::log(getUserIpAddr() . "Muliple users found and single set", ANALOG::URGENT);
                    echo("User not found!");
                    http_response_code(401);
                    exit();
                }

                if ((count($SearchResultO->results)  ==  1  || (count($SearchResultO->results) > 1 && $personM != 'ignore') ))
                {
                    $payloadO->User = $SearchResultO->results[0]->Ref;
                    $payload = json_encode(($payloadO));
                }

            } else {
                if ($personM != "ignore") {
                    Analog::log(getUserIpAddr() . "User not found", ANALOG::URGENT);
                    echo("User not found!");
                    http_response_code(401);
                    exit();
                }
            }

        } else {
            if ($personM != "ignore") {
                Analog::log(getUserIpAddr() . "User not found", ANALOG::URGENT);
                echo("User not found!");
                http_response_code(401);
                exit();
            }


        }


    }


    $response = $client->RunAction(-1, 'POST', $tokenInfo["CanCreateCall"] , '*', '*', $payload);
    $responseOB = json_decode($response);

   // $responseC = $client->RunAction(-1, 'PUT', '', '*', str_replace('api:v2/', '', $responseOB->_actions->Update[0]->href),'{"ShortDescription":"Bob Barker"}');


    if (isset($responseOB->Ref)) {
        $response = $client->RunAction(-1, 'PUT', '', '*', str_replace('api:v2/', '', $responseOB->_actions->Submit[0]->href), null);


    }


} else {
    Analog::log(getUserIpAddr() . "API Relay not online, org=" . $tokenInfo["INSTITUTION_ID"] . ",tokenID=" . $tokenInfo["ID"] . ", method=" . $method . ", resource=" . $resource . ", id=" . $id . ", url=" . $url . ", payload=" . (isset($payload)) ? $payload : "", Analog::URGENT);
    $response = '{"Error":"API Relay not online"}';
}

$client->Logout();

// Return Results to user
header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');
echo $response;

// Function to get remote IP address
function getUserIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

// Function to Init Analog Logging Service https://github.com/jbroadway/analog
function logInit($server, $db, $user, $password)
{
    $options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES => false,];


    $dsn = "mysql:host=" . $server . ";dbname=" . $db;

    try {
        $pdo = new \PDO($dsn, $user, $password, $options);
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
    $table = 'logs';
    // Helper method for creating the database table
    // Analog\Handler\PDO::createTable($pdo, $table);

    ///  Initialize Analog with your PDO connection and table
    Analog::handler(PDO::init($pdo, $table));


}