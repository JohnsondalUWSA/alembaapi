<?php

use Analog\Analog;
use Analog\Handler\PDO;

//Composer Load
require '../../vendor/autoload.php';

//Application Settings Load
require_once('../../settings.php');

//Encryption Key and Lib
require_once('../../lib/encrypt/key.php');
require_once('../../lib/encrypt/uws_encrypt.php');

//ASM Client Class
require_once('../../lib/asmClient.php');


//Retrieve Post Headers - Used to find bearer token
$headers = apache_request_headers();


//Post Variables
$resource = null;
$filter = null;
$select = null;

// Decrypt mySQL Password
$decodedPassword = uws_encrypt::encrypt_decrypt('decrypt', $mysql_password, $mykey);

// Init Analog Loggging Agent
logInit($mysql_server, $mysql_db, $mysql_user, $decodedPassword);

// Confirm that Auth Has been sent
if (!isset($headers["Authorization"])) {
    Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt", ANALOG::URGENT);
    http_response_code(401);
    exit();
}

// Confirm that resource was sent and set variable
if (!isset($_POST["resource"])) {
    echo("Missing Resource!");
    Analog::log(getUserIpAddr() . " Invaild Client Request - Missing Resource", ANALOG::DEBUG);
    http_response_code(400);
    exit();
} else $resource = $_POST["resource"];

// Confrim that select was sent and set variable
if (!isset($_POST["select"])) {
    echo("Missing select!");
    Analog::log(getUserIpAddr() . " Invaild Client Request - Missing Select", ANALOG::DEBUG);
    http_response_code(400);
    exit();
} else $select = $_POST["select"];

// Set Filter Variable, not requried
if (isset($_POST["filter"]))
    $filter = rawurlencode($_POST["filter"]);


// Set MySQL Info - https://github.com/SergeyTsalkov/meekrodb
DB::$host = $mysql_server;
DB::$user = $mysql_user;

DB::$password = $decodedPassword;
DB::$dbName = $mysql_db;


//encrypt token that has been set via Bearer auth.  DB stores the token in encrypted state.
$encryptedToken = uws_encrypt::encrypt_decrypt('encrypt', str_replace("Bearer ", "", $headers["Authorization"]), $mykey);

//Retrieve Token from DB using encryptedToken
$tokenInfo = DB::queryFirstRow("SELECT * FROM `tokens` where token = '" . $encryptedToken . "'");

$isTest = true;
if ($tokenInfo["isTest"] == "0")
    $isTest = false;

if (!$isTest) {
    $asm_auth = $asmclient["prod"]->auth;
    $asm_api = $asmclient["prod"]->api;
    $asm_username = $asmclient["prod"]->partitions[$tokenInfo["PartitionId"]]->username;
    $asm_password = $asmclient["prod"]->partitions[$tokenInfo["PartitionId"]]->password;
    $asm_client_id = $asmclient["prod"]->client_id;
} else {
    $asm_auth = $asmclient["test"]->auth;
    $asm_api = $asmclient["test"]->api;
    $asm_username = $asmclient["test"]->partitions[$tokenInfo["PartitionId"]]->username;
    $asm_password = $asmclient["test"]->partitions[$tokenInfo["PartitionId"]]->password;
    $asm_client_id = $asmclient["test"]->client_id;
}

//Is Token in DB?
if ($tokenInfo == null) {
    echo("Bad Token!");
    Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt - BAD Token", ANALOG::URGENT);
    http_response_code(401);
    exit();
}

//IS Token Active?
if ($tokenInfo["ACTIVE"] != "1") {
    Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt - Expired Token", ANALOG::URGENT);
    echo("Token is expired!");
    http_response_code(401);
    exit();
}

//Is Token IP SCOPED
if ($tokenInfo["IP_FILTER"] != "0.0.0.0") {

    $remoteIP = getUserIpAddr();
    if ($remoteIP == "::1")
        $remoteIP = '127.0.0.1';

    if ($remoteIP != $tokenInfo["IP_FILTER"]) {
        Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt - Invalid IP", ANALOG::URGENT);
        echo("No Access!");
        http_response_code(401);
        exit();
    }
}

$partition = "*";

if (!$isTest) {
//Check Scope of Request
    $tokenScope = DB::queryFirstRow("SELECT * FROM scopes where method = 'GET' and tokenID = '" . $tokenInfo["ID"] . "' and resource='" . $resource . "' and url='*'");


    if (!isset($tokenScope)) {
        Analog::log(getUserIpAddr() . " UnAuthorized Login Attempt - Missing Token Scope", ANALOG::URGENT);
        echo("No Access, no scope defined!");
        http_response_code(401);
        exit();
    }
    $partition = $tokenScope["PartitionId"];

}

$response = "";
// INIT ASM CLIENT - ASM Client will init login auth with Alemba
$client = new asmClient($asm_client_id, $asm_username,
    uws_encrypt::encrypt_decrypt('decrypt', $asm_password, $mykey),
    $asm_auth, $asm_api, $tokenInfo["INSTITUTION_ID"]);

// Log Users Query in DB
Analog::log(getUserIpAddr() . " org=" . $tokenInfo["INSTITUTION_ID"] . ", resoure=" . $resource . ", select=" . $select . ", filter=" . $filter, Analog::DEBUG);


// If client is connected run query, log if not online.
if ($client->connected)
    $response = $client->Query($select, $resource, $filter, $partition);
else {
    Analog::log(getUserIpAddr() . "org=" . $tokenInfo["INSTITUTION_ID"] . ", resoure=" . $resource . ", select=" . $select . ", filter=" . $filter, Analog::URGENT);
    $response = '{"Error":"API Relay not online"}';
}

$client->Logout();

// Return Results to user
header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');
echo $response;

// Function to get remote IP address
function getUserIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

// Function to Init Analog Logging Service https://github.com/jbroadway/analog
function logInit($server, $db, $user, $password)
{
    $options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES => false,];


    $dsn = "mysql:host=" . $server . ";dbname=" . $db;

    try {
        $pdo = new \PDO($dsn, $user, $password, $options);
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
    $table = 'logs';
    // Helper method for creating the database table
    // Analog\Handler\PDO::createTable($pdo, $table);

    ///  Initialize Analog with your PDO connection and table
    Analog::handler(PDO::init($pdo, $table));


}