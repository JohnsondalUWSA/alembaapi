<?php

require_once("../lib/encrypt/key.php");
require_once("../lib/encrypt/uws_encrypt.php");

//Generate a random string.
$token = openssl_random_pseudo_bytes(16);

//Convert the binary data into hexadecimal representation.
$token = bin2hex($token);

//Print it out for example purposes.
echo "decryptedToken:  ". $token;
echo "<br>";
echo "encryptedToken:  ". uws_encrypt::encrypt_decrypt('encrypt', $token,$mykey);