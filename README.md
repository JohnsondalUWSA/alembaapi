# UWS Alemba API Relay Service #

UWS is running a single tenant Alemba, utilizing organization and partitions units to restrict user data access.  Some institutions need to integrate existing tools with Alemba utilizing API’s, for services such as machine imaging. The relay service provides filtering to ensure data restrictions are kept int tact.
### What is this repository for? ###

* The Docs sub folder contains PDF & POSTMAN JSON collection.  
* The relay service handles both production & test ASM API calls.  Tokens are environment specfic and are proviced by the ASM admin team.
